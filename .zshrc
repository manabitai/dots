HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
HISTDUP=erase

setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS
setopt HIST_EXPIRE_DUPS_FIRST
setopt APPENDHISTORY
setopt SHAREHISTORY

# source plugins and theme
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/fzf/key-bindings.zsh

function p() {
    sh ~/.local/scripts/tmux-sessionizer.sh
}

alias v=nvim
alias r=ranger
alias c=clear
alias scratch='cd ~/Documents/Notes && nvim scratch.md'
alias notes='cd ~/Documents/Notes && nvim'
alias gpl='git pull'
alias gps='git push'
alias gc='git checkout'
alias gb='git branch'
alias gwa='git worktree add'
alias gwr='git worktree remove'
alias gwl='git worktree list'
alias gwp='git worktree prune'

# Delete merged branches
function gbdm {
    merged=$(git branch --merged | grep -Ev "(^\*|master|main|dev)")
    checked_out=$(echo $merged | grep -E "^\+")
    not_checked_out=$(echo $merged | grep -Ev "^\+")

    # Tell user if there are any merged branches
    # that are checked out elsewhere, e.g. worktree
    if [[ $checked_out ]]; then
        echo "checked out $checked_out"
    fi

    # Only run git branch -d when there are merged non-checked-out branches
    if [[ $not_checked_out ]]; then
        echo $not_checked_out | xargs git branch -d
    else
        echo "no non-checked-out merged branches"
    fi
}

# List all git stashes
function git-stashes() {
    cmd='
        stashed=$(git stash list)
        if [[ -n "$stashed" ]]; then
            echo "> $(pwd) > $(echo "$stashed" | wc -l)"
            echo "$stashed"
            echo ""
        fi
    '
    if [[ -z "$1" ]]; then
        find . -type d -name ".git" -execdir sh -c $cmd \;
    else
        find $1 -type d -name ".git" -execdir sh -c $cmd \;
    fi
}

# Credit to Andrew Burgess
# https://www.youtube.com/watch?v=DJR5RLVOjOw
function take {
    mkdir -p $1
    cd $1
}

# ignore case
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' '+m:{A-Z}={a-z}'

eval "$(starship init zsh)"

# enable autocompletion
autoload bashcompinit
autoload -Uz compinit
bashcompinit
compinit

# some options for better autocompletion
zstyle ':completion:*' menu select
zstyle ':completion::complete:*' gain-privileges 1

# autocompletion sources
source /usr/share/nvm/init-nvm.sh
complete -C "/usr/bin/aws_completer" aws

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"
[ -s "$HOME/.bun/_bun" ] && source "/home/martins/.bun/_bun" # completions

# ocaml
export PATH="$HOME/.opam/default/bin:$PATH"

# other
export PATH="$HOME/.cargo/bin:$HOME/.local/share/nvim/mason/bin:$HOME/.nimble/bin:$PATH"
export EDITOR="nvim"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.ghcup/bin:$PATH"
export PATH="$HOME/.go/bin:$PATH"

# golang
export GOPATH="$HOME/.go"

# pnpm
export PNPM_HOME="/home/martins/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
# pnpm end

# fly.io
export FLYCTL_INSTALL="/home/martins/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"

# ibus
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
