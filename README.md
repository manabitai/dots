# Dotfiles

# How to sync with $HOME

1. make sure this repo is in your home dir (eg. $HOME/dotfiles)
2. make sure you have GNU stow installed
3. run `stow .`, or `stow --adopt .` if there are conflicts
