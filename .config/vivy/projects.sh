#!/bin/sh

if [ -z "$1" ];
then
    projects=$(find ~/dotfiles ~/projects -maxdepth 2 -not -name '.git' -type d | cut -d '/' -f4-)
    for p in ${projects[@]}; do
        echo $p
    done
else
    kitty sh ~/.local/scripts/tmux-sessionizer.sh $2
fi
