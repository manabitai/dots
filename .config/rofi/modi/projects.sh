#!/usr/bin/env sh

projects=()

projects+=($(ls -dl ~/projects/dots/ | cut -d '/' -f5-))
projects+=($(ls -dl ~/projects/work/*/ | cut -d'/' -f5-))
projects+=($(ls -dl ~/projects/hobies/*/ | cut -d'/' -f5-))
projects+=($(ls -dl ~/projects/experiments/*/ | cut -d'/' -f5-))

if [ -z "$1" ]; then
    for p in ${projects[@]}; do
        echo $p
    done
else
    kitty -d "$HOME/projects/$1" -e "nvim" > /dev/null 2>&1 &
fi
