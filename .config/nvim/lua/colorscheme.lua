-- Expects a config folder
local home = os.getenv("HOME")
local filepath = home .. "/.config/nvim/config/colorscheme.txt"

---@param data { name: string, args: string }
local function set_colorscheme(data)
    -- Set the colorscheme
    vim.api.nvim_exec("colorscheme " .. data.args, false)

    -- Save the colorscheme to a file
    local file = assert(io.open(filepath, "w"))
    file:write(data.args)
    file:close()
end

local function load_colorscheme()
    -- Read last set colorscheme from config file
    local file = io.open(filepath, "r")
    if file == nil then return end
    local colorscheme = file:lines("*l")()
    file:close()
    -- Set the colorscheme
    vim.api.nvim_exec("colorscheme " .. colorscheme, false)
    vim.api.nvim_exec("highlight! default link Folded Comment", false)
    vim.api.nvim_exec("highlight! default link RustCommentLineDoc Comment", false)
    -- Set theme for lualine
    -- WARN: This is a bad way to do this. We basically initialize lualine for a second time.
    require("lualine").setup(require("plugins.lualine").opts)
end

vim.api.nvim_create_user_command(
    "Colorscheme",
    set_colorscheme,
    { nargs = 1, complete = "color" }
)

vim.api.nvim_create_autocmd({ "VimEnter" }, {
    pattern = { "*" },
    callback = load_colorscheme
})
