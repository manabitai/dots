-- Auto format files on write
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
    pattern = { "*.rs,*.lua,*.go,*.templ,*.svelte,*.ml,*.mli,*.odin" },
    callback = function()
        vim.lsp.buf.format(nil)
    end
})

-- Auto format files on write
vim.api.nvim_create_autocmd({ "BufWritePost" }, {
    pattern = { "*.ts,*.js,*.tsx,*.jsx,*.md" },
    command = ":FormatWrite"
})

-- Lint file and display issues
vim.api.nvim_create_autocmd({ "BufWritePost" }, {
    callback = function()
        require("lint").try_lint()
    end
})

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
    pattern = { "*.sh" },
    command = ":set filetype=sh",
})

vim.cmd [[
augroup AutoMoveQuickfix
    autocmd!
    autocmd FileType qf nnoremap <buffer> <silent> j :cnext<CR>:cc<CR>:copen<CR>
    autocmd FileType qf nnoremap <buffer> <silent> k :cprev<CR>:cc<CR>:copen<CR>
augroup END
]]
