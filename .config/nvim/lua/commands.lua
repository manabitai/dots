local lorem = [[
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum sem at dictum consectetur. Donec gravida lacus a lectus iaculis sagittis. Maecenas facilisis felis quis nunc blandit egestas eget pulvinar augue. Fusce arcu quam, accumsan ac est vitae, ultrices vulputate velit. Aliquam erat volutpat. In ornare tincidunt nisi. Fusce sit amet mi vitae nulla condimentum consequat. Vivamus in purus et erat venenatis semper sed in odio. Nunc porta libero non fringilla suscipit.
Sed pulvinar a augue a fermentum. Nam in justo eu lorem maximus blandit in id magna. Donec id sapien scelerisque, blandit dolor id, efficitur eros. Nulla facilisi. Praesent facilisis tellus vel dolor pulvinar ornare. Aliquam vel sagittis eros, vel mattis ante. Pellentesque sagittis nunc eget enim volutpat, at volutpat elit efficitur. Sed sit amet leo fermentum ante auctor dignissim. Suspendisse blandit tincidunt risus, vitae dignissim magna finibus sit amet. Vivamus eget neque vulputate orci varius cursus ut et augue. Quisque blandit porttitor magna at fringilla.
Quisque bibendum massa non eleifend tristique. Aliquam varius leo quis feugiat molestie. Pellentesque bibendum sollicitudin quam, quis lobortis diam dictum et. Integer non erat aliquam, commodo risus vel, varius nisl. Duis porttitor pharetra ex, vitae mollis nunc ornare eu. Pellentesque sagittis mauris eu.
]]

function table.slice(tbl, first, last, step)
    local sliced = {}

    for i = first or 1, last or #tbl, step or 1 do
        sliced[#sliced + 1] = tbl[i]
    end

    return sliced
end

local function nlorem(n)
    local words = {}
    for word in string.gmatch(lorem, "[^%s]+") do
        table.insert(words, word)
    end
    local start = math.random(0, #words - n)
    words = table.slice(words, start, start + n)
    return table.concat(words, " ")
end

vim.api.nvim_create_user_command('Lorem', function(opts)
    local n = opts.args
    local text = nlorem(tonumber(n))
    vim.api.nvim_put({ text }, 'c', true, true)
end, { nargs = 1 })
