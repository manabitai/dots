-- Diagnostics
vim.api.nvim_set_keymap("n", "[g", "<cmd>lua vim.diagnostic.goto_prev()<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "]g", "<cmd>lua vim.diagnostic.goto_next()<CR>", { noremap = true })

-- Terminal
vim.api.nvim_set_keymap("t", "<Esc>", "<C-\\><C-n>", { noremap = true })

-- QuickFix
vim.api.nvim_set_keymap("n", "[q", "<cmd>cprev<CR>zz", {})
vim.api.nvim_set_keymap("n", "]q", "<cmd>cnext<CR>zz", {})

vim.api.nvim_set_keymap("n", "<leader>ee", ":normal! oif err != nil {<Esc>o}<Esc>O", { noremap = true })

vim.cmd [[
    command! -range -nargs=0 ModsExplain :'<,'>w !mods explain this, be very succint
    command! -range -nargs=* ModsRefactor :'<,'>!mods refactor this to improve its readability
    command! -range -nargs=+ Mods :'<,'>w !mods <q-args>
]]

return {
    lsp = {
        { "<leader>ld", "<cmd>lua vim.lsp.buf.definition()<CR>" },
        { "<leader>lh", "<cmd>lua vim.lsp.buf.hover()<CR>" },
        { "<leader>li", "<cmd>lua vim.lsp.buf.implementation()<CR>" },
        { "<leader>lr", "<cmd>lua vim.lsp.buf.references()<CR>" },
        { "<leader>lc", "<cmd>lua vim.lsp.buf.code_action()<CR>" },
        { "<F2>",       "<cmd>lua vim.lsp.buf.rename()<CR>" },
    },
    toggleterm = {
        { "<C-l>", "<cmd>ToggleTerm direction=float<CR>" }
    },
    telescope = {
        { "<C-f>",      "<cmd>Telescope find_files hidden=true<CR>" },
        { "<leader>ds", "<cmd>Telescope lsp_document_symbols<CR>" },
        { "<leader>ws", "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>" },
        { "<leader>/",  "<cmd>Telescope live_grep<CR>" },
    },
    oil = {
        { "<leader>.", "<cmd>Oil<CR>" },
        { "<leader>-", "<cmd>Oil .<CR>" },
    },
    grug = {
        { "<leader>s", "<cmd>GrugFar<CR>" },
    }
}
