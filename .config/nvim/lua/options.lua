local o = vim.opt

o.number = true
o.relativenumber = true
o.spell = true
o.spelllang = { "en", "lv", "cjk" }
o.spellcapcheck = ""
o.autoindent = true
o.expandtab = true
o.tabstop = 4
o.shiftwidth = 4
o.smarttab = true
o.softtabstop = 4
o.mouse = "a"
o.wrap = false
o.termguicolors = true
o.ignorecase = true
o.scrolloff = 10
o.clipboard = "unnamedplus" -- TODO: may need to update (original was +=unnamedplus)
o.signcolumn = "yes"
o.spelloptions = 'camel'
o.listchars = {
    tab = "  "
}

-- Options for nvim-cmp
o.completeopt = "menu,menuone,noinsert,noselect"
o.shortmess = "csa"

-- Shorten update time so that diagnostic popup shows right away
o.updatetime = 300

-- Set <leader> key before running `setup` on packages
-- because otherwise some features would use the wrong key
vim.g.mapleader = " "
vim.g.neovide_hide_mouse_when_typing = false

-- Used primarily for GrugFar
vim.g.maplocalleader = ","

vim.filetype.add({
    extension = {
        templ = "templ",
        jel = "jel",
    },
    pattern = {
        [".env.*"] = "sh"
    }
})
