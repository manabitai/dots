local function file_exists(path)
    local f = io.open(path, "r")
    if f ~= nil then return true else return false end
end

local js = {
    biome = {
        "biome.json"
    },
    prettierd = {
        ".prettierrc",
        ".prettierrc.json",
        "prettier.config.js",
    },
    eslint_d = {
        ".eslintrc",
        ".eslintrc.json",
    },
}

local function jsconfig()
    local cwd = vim.fn.getcwd()
    local formaters = {}
    for formatter, files in pairs(js) do
        for _, file in ipairs(files) do
            if file_exists(cwd .. "/" .. file) then
                table.insert(formaters, require("formatter.filetypes.javascript")[formatter])
                break
            end
        end
    end
    return formaters
end

return {
    "mhartington/formatter.nvim",
    config = function()
        require("formatter").setup {
            filetype = {
                markdown = { require("formatter.filetypes.markdown").prettierd },
                javascript = jsconfig(),
                typescript = jsconfig(),
                javascriptreact = jsconfig(),
                typescriptreact = jsconfig(),
                sql = { require("formatter.filetypes.sql") },
                ["*"] = {
                    require("formatter.filetypes.any").remove_trailing_whitespace,
                },
            },
        }
    end
}
