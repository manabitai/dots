return {
    "nvim-lualine/lualine.nvim",
    dependencies = {
        { "folke/tokyonight.nvim", branch = "main" },
    },
    opts = {
        options = {
            icons_enabled = true,
            theme = "auto",
        },
        sections = {
            lualine_a = { "mode" },
            lualine_b = {
                {
                    "diagnostics",
                    sources = { "nvim_diagnostic", "ale", "coc" },
                    sections = { "error", "warn", "info", "hint" }
                },
                "diff"
            },
            lualine_c = { "filename" },
            lualine_x = {},
            lualine_y = { "location" },
            lualine_z = { "branch" },
        }
    },
}
