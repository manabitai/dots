return {
    -------------------------
    --- UI and Workflow -----
    -------------------------
    "ryanoasis/vim-devicons",
    "kyazdani42/nvim-web-devicons",
    { "lewis6991/gitsigns.nvim",  config = true, },
    { "folke/todo-comments.nvim", config = true },
    {
        "MagicDuck/grug-far.nvim",
        config = true,
        keys = require("keys").grug
    },
    {
        "ggandor/leap.nvim",
        config = function()
            require("leap").add_default_mappings()
        end,
    },
    {
        "olimorris/persisted.nvim",
        enabled = false,
        opts = {
            autoload = true,
            use_git_branch = false,
            allowed_dirs = {
                "~/dotfiles",
                "~/projects/work",
                "~/projects/personal",
                "~/projects/experiments",
            },
        },
    },
    {
        'stevearc/oil.nvim',
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {
            view_options = {
                show_hidden = true,
            },
        },
        keys = require("keys").oil,
    },

    -------------------------
    --- Color Scheme --------
    -------------------------
    { "folke/tokyonight.nvim", branch = "main" },
    { "catppuccin/nvim",       name = "catppuccin" },
    "ayu-theme/ayu-vim",
    "joshdick/onedark.vim",

    -------------------------
    --- Helpers -------------
    -------------------------
    "nvim-lua/plenary.nvim",
    { "windwp/nvim-autopairs", config = true, },
    "preservim/nerdcommenter",
    {
        "iamcco/markdown-preview.nvim",
        ft = "markdown",
        build = "cd app && yarn install",
    },
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("nvim-treesitter.configs").setup {
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = true
                }
            }

            local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
            parser_config.templ = {
                install_info = {
                    url = "https://github.com/vrischmann/tree-sitter-templ.git",
                    files = { "src/parser.c", "src/scanner.c" },
                    branch = "master",
                },
            }

            vim.treesitter.language.register("templ", "templ")

            parser_config.jel = {
                install_info = {
                    url = "~/projects/personal/jel/tree-sitter",
                    files = { "src/parser.c" },
                    branch = "main",
                    generate_requires_npm = false,
                    requires_generate_from_grammar = false,
                },
            }

            vim.treesitter.language.register("jel", "jel")
        end
    },

    -------------------------------------------------------
    --- Language server configuration and utilities -------
    -------------------------------------------------------
    "vrischmann/tree-sitter-templ",
    { "rust-lang/rust.vim",    ft = "rust" },
    {
        "simrat39/rust-tools.nvim",
        opts = {
            tools = {
                autoSetHints = true,
                inlay_hints = {
                    show_parameter_hints = false,
                    parameter_hints_prefix = "",
                    other_hints_prefix = "",
                }
            },
            server = {
                settings = {
                    ["rust-analyzer"] = {
                        -- Enable clippy on save
                        checkOnSave = {
                            command = "clippy"
                        },
                    }
                }
            },
            dap = {
                adapter = {
                    type = "executable",
                    command = "lldb-vscode",
                    name = "rt_lldb",
                }
            }
        },
        ft = "rust",
    }
}
