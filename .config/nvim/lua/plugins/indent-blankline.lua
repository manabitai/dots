return {
    "lukas-reineke/indent-blankline.nvim", -- Indent Guide
    main = "ibl",
    config = function()
        vim.opt.list = true

        require("ibl").setup {
            scope = {
                show_start = false,
                highlight = "Conditional"
            },
            indent = {
                tab_char = "▎"
            },
            --space_char_blankline = " ",
            --show_current_context = true,
            --show_trailing_blankline_indent = false,
        }
    end,
}
