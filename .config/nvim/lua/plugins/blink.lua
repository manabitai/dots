return {
    "saghen/blink.cmp",
    lazy = false,
    version = "v0.*",
    ---@module "blink.cmp"
    ---@type blink.cmp.Config
    opts = {
        keymap = { preset = "super-tab" },
        --windows = {
        --    --documentation = { auto_show = true },
        --},
        sources = {
            default = { "lsp", "path", "buffer" }
        }
    }
}
