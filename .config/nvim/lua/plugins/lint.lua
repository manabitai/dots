local function file_exists(path)
    local f = io.open(path, "r")
    if f ~= nil then return true else return false end
end

local function jsconfig()
    local cwd = vim.fn.getcwd()
    local linters = {}

    if file_exists(cwd .. "/.eslintrc") or file_exists(cwd .. "/.eslintrc.json") then
        table.insert(linters, "eslint_d")
    end

    if file_exists(cwd .. "/biome.json") then
        table.insert(linters, "biomejs")
    end

    return linters
end

return {
    "mfussenegger/nvim-lint",
    config = function()
        require("lint").linters_by_ft = {
            javascript = jsconfig(),
            typescript = jsconfig(),
            sql = { "sqlfluff" },
            css = { "stylelint" },
            scss = { "stylelint" },
            sass = { "stylelint" },
        }
    end
}
