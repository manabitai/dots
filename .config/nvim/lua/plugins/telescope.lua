return {
    "nvim-telescope/telescope.nvim",
    dependencies = {
        {
            "nvim-telescope/telescope-fzf-native.nvim",
            priority = 1000,
            build = { "make" },
        },
    },
    keys = require("keys").telescope,
    config = function()
        local telescope = require("telescope")

        telescope.setup {
            defaults = {
                file_ignore_patterns = {
                    "node_modules/",
                    "dist/",
                    "target/",
                    ".git/",
                    "coverage/",
                    "bruno/"
                }
            },
            extensions = {
                file_browser = {
                    hijack_netrw = true,
                }
            }
        }

        telescope.load_extension("fzf")
    end
}
