return {
    {
        "neovim/nvim-lspconfig",
        lazy = false,
        dependencies = {
            { "williamboman/mason.nvim",           config = true, },
            { "williamboman/mason-lspconfig.nvim", config = true, },
            { "saghen/blink.cmp" },
        },
        keys = require("keys").lsp,
        event = "FileType",
        config = function()
            local nvim_lsp = require("lspconfig")
            local mason_lspconfig = require("mason-lspconfig")
            local capabilities = require("blink.cmp").get_lsp_capabilities()

            mason_lspconfig.setup_handlers({
                function(server_name)
                    nvim_lsp[server_name].setup { capabilities = capabilities }
                end,
                ["lua_ls"] = function()
                    nvim_lsp.lua_ls.setup {
                        capabilities = capabilities,
                        settings = {
                            Lua = {
                                runtime = {
                                    -- Tell the language server which version of Lua you"re using (most likely LuaJIT in the case of Neovim)
                                    version = "LuaJIT",
                                },
                                diagnostics = {
                                    -- Get the language server to recognize the `vim` global
                                    globals = { "vim", "P" },
                                },
                                workspace = {
                                    -- Make the server aware of Neovim runtime files
                                    library = vim.api.nvim_get_runtime_file("", true),
                                },
                                -- Disable documentation comment coloring
                                semantic = { enable = false },
                                -- Do not send telemetry data containing a randomized but unique identifier
                                telemetry = {
                                    enable = false,
                                },
                            },
                        },
                    }
                end,
                ["tailwindcss"] = function()
                    nvim_lsp.tailwindcss.setup {
                        capabilities = capabilities,
                        filetypes = { "templ", "javascriptreact", "typescriptreact", "html", "pug", "astro", "html.handlebars", "svelte" },
                        init_options = {
                            userLanguages = {
                                templ = "html",
                                pug = "html",
                                astro = "html",
                                svelte = "html",
                                html = "html",
                                ["html.handlebars"] = "html",
                            }
                        }
                    }
                end,
                ["emmet_ls"] = function()
                    nvim_lsp.emmet_ls.setup({
                        capabilities = capabilities,
                        filetypes = { "css", "eruby", "html", "javascript", "javascriptreact", "less", "sass", "scss", "svelte", "pug", "typescriptreact", "vue" },
                        init_options = {
                            html = {
                                options = {
                                    -- For possible options, see: https://github.com/emmetio/emmet/blob/master/src/config.ts#L79-L267
                                    ["bem.enabled"] = true,
                                },
                            },
                        }
                    })
                end
            })

            nvim_lsp.templ.setup { capabilities = capabilities }
        end,
    },
}
