vim.cmd("filetype plugin on")

require("options")
require("commands")
require("keys")
require("autocmd")
require("install_lazy")
require("colorscheme")
require("lazy").setup("plugins")

-- Change default signcolumn icons
vim.cmd("sign define DiagnosticSignError   text= texthl=DiagnosticSignError   linehl=               numhl=")
vim.cmd("sign define DiagnosticSignWarning text= texthl=DiagnosticSignWarning linehl=               numhl=")
vim.cmd("sign define DiagnosticSignInfo    text= texthl=DiagnosticSignInfo    linehl=               numhl=")
vim.cmd("sign define DiagnosticSignHint    text= texthl=DiagnosticSignHint    linehl=               numhl=")
