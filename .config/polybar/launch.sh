#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Launch bar for all monitors
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload thin 2>~/.config/polybar/log &
done
