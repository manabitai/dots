#!/usr/bin/sh

bat=$(cat /sys/class/power_supply/BAT0/capacity)
volume=$(amixer sget Master | awk -F"[][]" '/Left:/ { print $2 }')
date=$(date +'%b %d %a %H:%M')

echo "  $bat% |   $volume |  $date"
